# ecFlow: Planificador para gestión y monitorización de procesos

El objetivo de este curso es familiarizarse con el entorno del planificador ecFlow del ECMWF para la gestión y 
monitorización de procesos multiplataforma como pueden ser: captación de datos diarios, 
ejecución de modelos numéricos, compilaciones en remoto, instalación de máquinas 
virtuales, gestión de entornos operacionales, etc… 



## Modo Texto

Profesor: Daniel Santos

* [01 - Curso ecFlow I](http://nbviewer.ipython.org/urls/bitbucket.org/tmorales/curso-ecflow/raw/4317cd0bcd246d0f30b2be89b61ffa6a976ec59e/Tutorial_ecFlow_modo_texto/Curso%20ecFlow%20I.ipynb)
* [02 - Curso ecFlow II](http://nbviewer.ipython.org/urls/bitbucket.org/tmorales/curso-ecflow/raw/b0510a751b1c6f7446eadd756cce6a3859f9c526/Tutorial_ecFlow_modo_texto/Curso%20ecFlow%20II.ipynb)
* [03 - Curso ecFlow III](http://nbviewer.ipython.org/urls/bitbucket.org/tmorales/curso-ecflow/raw/313251f665d675dcf1b64f4fc2a908d78df30f16/Tutorial_ecFlow_modo_texto/Curso%20ecFlow%20III.ipynb)



 

## Python API

Profesor: Tomás Morales


### Introducción


* [01 - Introducción a Python](http://nbviewer.ipython.org/urls/bitbucket.org/tmorales/curso-ecflow/raw/202055f0f9b2a18450e1018b4c6cf7c0483fa5be/Introduccion/01%20Introduccion%20Python.ipynb)
* [02 - Introdiducción Cliente Servidor](http://nbviewer.ipython.org/urls/bitbucket.org/tmorales/curso-ecflow/raw/202055f0f9b2a18450e1018b4c6cf7c0483fa5be/Introduccion/02%20Introduccion%20Cliente%20Servidor%20.ipynb)



### Lecciones


* [01 - Definición de una Suite I](http://nbviewer.ipython.org/urls/bitbucket.org/tmorales/curso-ecflow/raw/d83d4b934c3e12a23051e2c42eae8eca22762f52/Tutorial_ecFlow/Definicion%20de%20una%20Suite%20I.ipynb)
* [02 - Definición de una Suite II](http://nbviewer.ipython.org/urls/bitbucket.org/tmorales/curso-ecflow/raw/202055f0f9b2a18450e1018b4c6cf7c0483fa5be/Tutorial_ecFlow/Definicion%20de%20una%20Suite%20II.ipynb)
* [03 - Definición de una Suite III](http://nbviewer.ipython.org/urls/bitbucket.org/tmorales/curso-ecflow/raw/202055f0f9b2a18450e1018b4c6cf7c0483fa5be/Tutorial_ecFlow/Definicion%20de%20una%20Suite%20III.ipynb)
* [03 - Someter una Suite](http://nbviewer.ipython.org/urls/bitbucket.org/tmorales/curso-ecflow/raw/202055f0f9b2a18450e1018b4c6cf7c0483fa5be/Tutorial_ecFlow/Someter%20Suite.ipynb) 
* [04 - Eliminar uns Suite](http://nbviewer.ipython.org/urls/bitbucket.org/tmorales/curso-ecflow/raw/202055f0f9b2a18450e1018b4c6cf7c0483fa5be/Tutorial_ecFlow/Eliminar%20Suite.ipynb)
* [05 - Trigger, eventos, completados, meter y dependencias](http://nbviewer.ipython.org/urls/bitbucket.org/tmorales/curso-ecflow/raw/202055f0f9b2a18450e1018b4c6cf7c0483fa5be/Tutorial_ecFlow/Trigger%2C%20eventos%2C%20completados%2C%20meter%20y%20dependencias.ipynb)
* [06 - Label, Repeat y Limit](http://nbviewer.ipython.org/urls/bitbucket.org/tmorales/curso-ecflow/raw/202055f0f9b2a18450e1018b4c6cf7c0483fa5be/Tutorial_ecFlow/Label%2C%20Repeat%20y%20Limit.ipynb)


## Ejercicios resueltos

* [01 - Ejercicio 1](http://nbviewer.ipython.org/urls/bitbucket.org/tmorales/curso-ecflow/raw/b2d5287a2eb5515fcfc5d15dbd35b5b462cbe5ee/EjerciciosResueltos/Ejercicio%201.ipynb)


## Notas

* [01 - Conectarse al Servidor del Curso](http://nbviewer.ipython.org/urls/bitbucket.org/tmorales/curso-ecflow/raw/639c43bf4136b48d88c05ff260ae57f572cbb419/Notas/Servidores%20curso.ipynb)



notebook realizados por T. Morales y D. Santos